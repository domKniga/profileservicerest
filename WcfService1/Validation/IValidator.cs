﻿using System.Collections.Generic;
using UserProfileService.Validation.Rules;

namespace UserProfileService.Validation
{
    public interface IValidator
    {
        ISet<IRule> Rules { get; }

        IValidator IncludeRuleset(ISet<IRule> ruleset);
        IValidator AppendRule(IRule rule);
        IValidator Initialize();

        bool Validate(string parameter);
    }
}
