﻿namespace UserProfileService.Validation.Rules
{
    public class FirstLetterUppercaseRule : IRule
    {
        public string FailureMessage
        {
            get
            {
                return "Невалиден формат на данните! Първата буква трябва да е главна за имена.";
            }
        }

        public bool Test(string parameter)
        {
            return char.IsUpper(parameter[0]);
        }
    }
}