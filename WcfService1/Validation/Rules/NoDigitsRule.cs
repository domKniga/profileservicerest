﻿using System.Linq;

namespace UserProfileService.Validation.Rules
{
    public class NoDigitsRule : IRule
    {
        public string FailureMessage
        {
            get
            {
                return "Невалиден формат на данните! Позволените символи са: букви";
            }
        }

        public bool Test(string parameter)
        {
            return !(parameter.Any(character => char.IsDigit(character)));
        }
    }
}