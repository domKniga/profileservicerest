﻿using System.Linq;

namespace UserProfileService.Validation.Rules
{
    public class NoSymbolsRule : IRule
    {
        public string FailureMessage
        {
            get
            {
                return "Невалиден формат на данните! Не са позволени специални символи.";
            }
        }

        public bool Test(string parameter)
        {
            return !(parameter.ToArray().Any(character => char.IsSymbol(character)));
        }
    }
}