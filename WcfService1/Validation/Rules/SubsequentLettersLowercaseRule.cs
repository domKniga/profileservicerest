﻿using System.Linq;

namespace UserProfileService.Validation.Rules
{
    public class SubsequentLettersLowercaseRule : IRule
    {
        public string FailureMessage
        {
            get
            {
                return "Невалиден формат на данните - само първата буква трябва да е главна!";
            }
        }

        public bool Test(string parameter)
        {
            return parameter.ToCharArray().ToList().GetRange(1, (parameter.Length - 1)).All(letter => char.IsLower(letter));
        }
    }
}