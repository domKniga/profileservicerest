﻿namespace UserProfileService.Validation.Rules
{
    public class NoNullRule : IRule
    {
        public string FailureMessage
        {
            get
            {
                return "Липса на въведени данни! Моля въведете данни и опитайте пак.";
            }
        }

        public bool Test(string parameter)
        {
            return (!string.IsNullOrEmpty(parameter));
        }
    }
}