﻿using System;
using System.Linq;
using static UserProfileService.Model.GenderEnum;

namespace UserProfileService.Validation.Rules
{
    public class GenderPresentInEnumRule : IRule
    {
        public string FailureMessage
        {
            get
            {
               return "Невалиден формат на данните! Позволените символи са: ('Mъж', 'Жена')!";
            }
        }

        public bool Test(string parameter)
        {
            Genders[] genders = (Genders[])Enum.GetValues(typeof(Genders));

            return genders.Any(gender => string.Equals(parameter, gender.stringValue()));
        }
    }
}