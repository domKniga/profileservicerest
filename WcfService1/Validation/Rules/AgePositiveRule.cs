﻿namespace UserProfileService.Validation.Rules
{
    public class AgePositiveRule : IRule
    {
        public string FailureMessage
        {
            get
            {
                return "Невалиден формат на данните! Не са позволени отрицателни или нулеви стойности.";
            }
        }

        public bool Test(string parameter)
        {
            return (short.Parse(parameter) > 0);
        }
    }
}