﻿namespace UserProfileService.Validation.Rules
{
    public class PersonalNumberLengthRule : IRule
    {
        protected const int PERSONAL_NUMBER_LENGTH = 10;

        public string FailureMessage
        {
            get
            {
                return "Невалиден формат на данните! Позволената дължина за ЕГН е: " + PERSONAL_NUMBER_LENGTH;
            }
        }

        public bool Test(string parameter)
        {
            return (parameter.Length == PERSONAL_NUMBER_LENGTH); ;
        }
    }
}