﻿namespace UserProfileService.Validation.Rules
{
    public interface IRule
    {
        string FailureMessage { get; }

        bool Test(string parameter);
    }
}
