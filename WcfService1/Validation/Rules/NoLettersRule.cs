﻿using System.Linq;

namespace UserProfileService.Validation.Rules
{
    public class NoLettersRule : IRule
    {
        public string FailureMessage
        {
            get
            {
                return "Невалиден формат на данните! Позволените символи са: цифри";
            }
        }

        public bool Test(string parameter)
        {
            return !(parameter.Any(character => char.IsLetter(character)));
        }
    }
}