﻿using System.IO;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using UserProfileService.Validation.Rules;

namespace UserProfileService.Validation
{
    public class ProfileValidator : IValidator
    {
        /// <summary>
        /// Static clas to for standard rulesets.
        /// </summary>
        public static class StandardRulesets
        {
            public static readonly ISet<IRule> NUMERIC_VALUE_RULESET = new HashSet<IRule>()
            {
                new NoSymbolsRule(),
                new NoLettersRule()
            };

            public static readonly ISet<IRule> NAME_VALUE_RULESET = new HashSet<IRule>()
            {
                new NoSymbolsRule(),
                new NoDigitsRule(),
                new FirstLetterUppercaseRule(),
                new SubsequentLettersLowercaseRule()
            };
        }

        #region CONSTANTS
        protected const string FALLBACK_VALIDATION_ERROR = "Проблем с валидацията от страна на сървъра!";

        #endregion

        #region PROPERTIES
        private ISet<IRule> _rules;
        public ISet<IRule> Rules
        {
            get
            {
                return _rules;
            }

            private set
            {
                _rules = value;
            }
        }
        #endregion

        public ProfileValidator()
        {
            _rules = new HashSet<IRule>();
            Rules.Add(new NoNullRule());
        }

        #region METHODS
        public IValidator Initialize()
        {
            Rules.Clear();
            Rules.Add(new NoNullRule());

            return this;
        }

        public IValidator IncludeRuleset(ISet<IRule> ruleset)
        {
            Rules.UnionWith(ruleset);

            return this;
        }

        public IValidator AppendRule(IRule rule)
        {
            Rules.Add(rule);

            return this;
        }

        public bool Validate(string parameter)
        {
            string errorReport = string.Empty;

            if (!RulesSpecified())
            {
                errorReport = FALLBACK_VALIDATION_ERROR;
                return false;
            }

            IEnumerable<IRule> brokenRules = Rules.Where(rule => !(rule.Test(parameter)));
            if (brokenRules.Count() > 0)
            {
                StringBuilder errorReportBuilder = new StringBuilder();

                brokenRules.ToList().ForEach(brokenRule => errorReportBuilder.Append(brokenRule.FailureMessage + ";"));

                errorReport = FinalizeErrorReport(errorReportBuilder, parameter);

                return false;
            }

            return true;
        }

        private bool RulesSpecified()
        {
            return (Rules.Count > 0);
        }

        protected string FinalizeErrorReport(StringBuilder errorReportBuilder, string parameter)
        {
            // Prepend error header(for debugging purposes)
            errorReportBuilder.Insert(0, "Bъведена стойност: [" + parameter + "];");

            string finalErrorReport = errorReportBuilder.ToString();

            LogErrorReport(finalErrorReport);

            return finalErrorReport;
        }

        protected void LogErrorReport(string finalErrorReport)
        {
            File.WriteAllLines(ProfileService.SERVICE_FAILURE_LOG, finalErrorReport.Split(';'));
        }
        #endregion
    }
}