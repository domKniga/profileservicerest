﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using UserProfileService.Model;
using UserProfileService.Validation;
using UserProfileService.Validation.Rules;

namespace UserProfileService
{
    public class ProfileService : IProfileService
    {
        #region CONSTANTS
        public static readonly IValidator VALIDATOR = new ProfileValidator();
        public static readonly string SERVICE_BASE_URL = @"http://localhost:58164/ProfileService.svc/";
        public static readonly string SERVICE_FAILURE_LOG = @"C:\Users\aswor\OneDrive\Documents\Visual Studio 2015\Projects\WcfService1\DemoUserProfileService\Resources\errorLog.txt";
        #endregion

        public ProfileService()
        {
        }

        #region HTTP GET METHODS
        public List<Profile> GetAll()
        {
            using (ProfilesModel dbModel = new ProfilesModel())
            {
                return dbModel.Profiles.ToList();
            }
        }

        public List<Profile> GetSpecific(string personalNumber)
        {
            bool validParameter = VALIDATOR
                                    .Initialize()
                                    .IncludeRuleset(ProfileValidator.StandardRulesets.NUMERIC_VALUE_RULESET)
                                    .AppendRule(new PersonalNumberLengthRule())
                                    .Validate(personalNumber);

            if (!validParameter)
            {
                return null;
            }

            using (ProfilesModel dbModel = new ProfilesModel())
            {
                return dbModel.Profiles.Where(profile => string.Equals(profile.personalNumber, personalNumber)).ToList();
            }
        }

        public List<Profile> GetAllForLastName(string lastName)
        {
            bool validParameter = VALIDATOR
                                    .Initialize()
                                    .IncludeRuleset(ProfileValidator.StandardRulesets.NAME_VALUE_RULESET)
                                    .Validate(lastName);

            if (!validParameter)
            {
                return null;
            }

            using (ProfilesModel dbModel = new ProfilesModel())
            {
                return dbModel.Profiles.Where(profile => string.Equals(profile.lastName, lastName)).ToList();
            }
        }

        public List<Profile> GetAllForGender(string genderName)
        {
            bool validParameter = VALIDATOR
                                    .Initialize()
                                    .IncludeRuleset(ProfileValidator.StandardRulesets.NAME_VALUE_RULESET)
                                    .AppendRule(new GenderPresentInEnumRule())
                                    .Validate(genderName);

            if (!validParameter)
            {
                return null;
            }

            using (ProfilesModel dbModel = new ProfilesModel())
            {
                return dbModel.Profiles.Where(profile => string.Equals(profile.gender, genderName)).ToList();
            }
        }
        #endregion

        #region HTTP POST METHODS
        public ProfileResponse AddProfile(ProfileRequest data)
        {
            ProfileResponse response = new ProfileResponse();

            if (!ValidateRequest(data))
            {
                response.code = ResponseCode.Error;
                response.description = File.ReadAllText(SERVICE_FAILURE_LOG);
                return response;
            }

            using (ProfilesModel dbModel = new ProfilesModel())
            {
                Profile newProfile = new Profile();
                newProfile.personalNumber = data.personalNumber;
                newProfile.firstName = data.firstName;
                newProfile.lastName = data.lastName;
                newProfile.gender = data.gender;
                newProfile.age = short.Parse(data.age);
                newProfile.phoneNumber = string.IsNullOrEmpty(data.phoneNumber) ? "ЛИПСВА" : data.phoneNumber;
                newProfile.homeAddress = string.IsNullOrEmpty(data.homeAddress) ? "ЛИПСВА" : data.homeAddress;

                try
                {
                    dbModel.Profiles.Add(newProfile);

                    try
                    {
                        dbModel.SaveChanges();
                    }
                    catch (OptimisticConcurrencyException)
                    {
                        ObjectContext context = ((IObjectContextAdapter)dbModel).ObjectContext;
                        List<object> refreshableObjects = dbModel.ChangeTracker.Entries().Select(c => c.Entity).ToList();
                        context.Refresh(RefreshMode.ClientWins, dbModel.Profiles);

                        dbModel.SaveChanges();
                    }

                    response.code = ResponseCode.Ok;
                    response.description = "Успешно добавен профил!";
                }
                catch (DbUpdateException dbUpdateExc)
                {
                    response.code = ResponseCode.Error;
                    response.description = "Проблем при запазването на профила :: вече има запис с това ЕГН!";

                    File.WriteAllLines(SERVICE_FAILURE_LOG, new string[] { dbUpdateExc.Message });
                }
                catch (Exception genericExc)
                {
                    response.code = ResponseCode.Error;
                    response.description = "Проблем при запазването на профила!";

                    File.WriteAllLines(SERVICE_FAILURE_LOG, new string[] { genericExc.Message });
                }
            }

            return response;
        }

        //Should be PUT method instead of POST
        public ProfileResponse UpdateProfile(ProfileRequest data)
        {
            ProfileResponse response = new ProfileResponse();

            if (!ValidateRequest(data))
            {
                response.code = ResponseCode.Error;
                response.description = File.ReadAllText(SERVICE_FAILURE_LOG);
                return response;
            }

            using (ProfilesModel dbModel = new ProfilesModel())
            {
                Profile updatedProfile = new Profile();
                updatedProfile.personalNumber = data.personalNumber;
                updatedProfile.firstName = data.firstName;
                updatedProfile.lastName = data.lastName;
                updatedProfile.gender = data.gender;
                updatedProfile.age = short.Parse(data.age);
                updatedProfile.phoneNumber = string.IsNullOrEmpty(data.phoneNumber) ? "ЛИПСВА" : data.phoneNumber;
                updatedProfile.homeAddress = string.IsNullOrEmpty(data.homeAddress) ? "ЛИПСВА" : data.homeAddress;

                try
                {
                    dbModel.Profiles.Attach(updatedProfile);
                    dbModel.Entry(updatedProfile).State = EntityState.Modified;

                    try
                    {
                        dbModel.SaveChanges();
                    }
                    catch (OptimisticConcurrencyException)
                    {
                        ObjectContext context = ((IObjectContextAdapter)dbModel).ObjectContext;
                        List<object> refreshableObjects = dbModel.ChangeTracker.Entries().Select(c => c.Entity).ToList();
                        context.Refresh(RefreshMode.ClientWins, dbModel.Profiles);

                        dbModel.SaveChanges();
                    }

                    response.code = ResponseCode.Ok;
                    response.description = "Успешно променен профил!";
                }
                catch (DbUpdateException dbUpdateExc)
                {
                    response.code = ResponseCode.Error;
                    response.description = "Проблем при запазването на профила :: вече има запис с това ЕГН!";

                    File.WriteAllLines(SERVICE_FAILURE_LOG, new string[] { dbUpdateExc.Message });
                }
                catch (Exception dbExc)
                {
                    response.code = ResponseCode.Error;
                    response.description = "Проблем при запазването на промените по профила!";

                    File.WriteAllLines(SERVICE_FAILURE_LOG, new string[] { dbExc.Message });
                }
            }

            return response;
        }

        //Should be DELETE method instead of POST
        public ProfileResponse DeleteProfile(string personalNumber)
        {
            ProfileResponse response = new ProfileResponse();

            bool validParameter = VALIDATOR
                          .Initialize()
                          .IncludeRuleset(ProfileValidator.StandardRulesets.NUMERIC_VALUE_RULESET)
                          .AppendRule(new PersonalNumberLengthRule())
                          .Validate(personalNumber);

            if (!validParameter)
            {
                response.code = ResponseCode.Error;
                response.description = File.ReadAllText(SERVICE_FAILURE_LOG);
                return response;
            }

            using (ProfilesModel dbModel = new ProfilesModel())
            {
                Profile profileToRemove = dbModel.Profiles.Find(personalNumber);

                try
                {
                    dbModel.Profiles.Remove(profileToRemove);

                    try
                    {
                        dbModel.SaveChanges();
                    }
                    catch (OptimisticConcurrencyException)
                    {
                        ObjectContext context = ((IObjectContextAdapter)dbModel).ObjectContext;
                        List<object> refreshableObjects = dbModel.ChangeTracker.Entries().Select(c => c.Entity).ToList();
                        context.Refresh(RefreshMode.ClientWins, dbModel.Profiles);

                        dbModel.SaveChanges();
                    }

                    response.code = ResponseCode.Ok;
                    response.description = "Успешно премахнат профил!";
                }
                catch (Exception dbExc)
                {
                    response.code = ResponseCode.Error;
                    response.description = "Проблем при премахването на профила!";

                    File.WriteAllLines(SERVICE_FAILURE_LOG, new string[] { dbExc.Message });
                }
            }

            return response;
        }
        #endregion

        private bool ValidateRequest(ProfileRequest data)
        {
            bool validPersonalNumber = VALIDATOR
                                   .Initialize()
                                   .IncludeRuleset(ProfileValidator.StandardRulesets.NUMERIC_VALUE_RULESET)
                                   .AppendRule(new PersonalNumberLengthRule())
                                   .Validate(data.personalNumber);
            if (!validPersonalNumber) return validPersonalNumber;

            bool validFirstName = VALIDATOR
                                .Initialize()
                                .IncludeRuleset(ProfileValidator.StandardRulesets.NAME_VALUE_RULESET)
                                .Validate(data.firstName);
            if (!validFirstName) return validFirstName;

            bool validLastName = VALIDATOR
                                .Initialize()
                                .IncludeRuleset(ProfileValidator.StandardRulesets.NAME_VALUE_RULESET)
                                .Validate(data.lastName);
            if (!validLastName) return validLastName;

            bool validAge = VALIDATOR
                                .Initialize()
                                .IncludeRuleset(ProfileValidator.StandardRulesets.NUMERIC_VALUE_RULESET)
                                .AppendRule(new AgePositiveRule())
                                .Validate(data.age);
            if (!validAge) return validAge;

            bool validGender = VALIDATOR
                               .Initialize()
                               .IncludeRuleset(ProfileValidator.StandardRulesets.NAME_VALUE_RULESET)
                               .AppendRule(new GenderPresentInEnumRule())
                               .Validate(data.gender);
            if (!validGender) return validGender;

            return true;
        }
    }
}
