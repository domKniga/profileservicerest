﻿using System.ServiceModel;
using System.ServiceModel.Web;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UserProfileService.Model;

namespace UserProfileService
{
    public enum ResponseCode
    {
        Ok = 0,
        Error = 1
    }

    [ServiceContract]
    public interface IProfileService
    {
        // GET methods
        [OperationContract]
        [WebInvoke(UriTemplate = "GetAll", Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        List<Profile> GetAll();

        [OperationContract]
        [WebInvoke(UriTemplate = "GetSpecific?personalNumber={personalNumber}", Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        List<Profile> GetSpecific(string personalNumber);

        [OperationContract]
        [WebInvoke(UriTemplate = "GetAllForLastName?lastName={lastName}", Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        List<Profile> GetAllForLastName(string lastName);

        [OperationContract]
        [WebInvoke(UriTemplate = "GetAllForGender?genderName={genderName}", Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        List<Profile> GetAllForGender(string genderName);

        //POST methods
        [OperationContract]
        [WebInvoke(UriTemplate = "AddProfile", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ProfileResponse AddProfile(ProfileRequest data);

        //Should be PUT method instead of POST
        [OperationContract]
        [WebInvoke(UriTemplate = "UpdateProfile", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ProfileResponse UpdateProfile(ProfileRequest data);

        //Should be DELETE method instead of POST
        [OperationContract]
        [WebInvoke(UriTemplate = "DeleteProfile?personalNumber={personalNumber}", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        ProfileResponse DeleteProfile(string personalNumber);
    }

    [DataContract]
    public class ProfileRequest
    {
        [DataMember]
        public string personalNumber { get; set; }

        [DataMember]
        public string firstName { get; set; }

        [DataMember]
        public string lastName { get; set; }

        [DataMember]
        public string gender { get; set; }

        [DataMember]
        public string age { get; set; }

        [DataMember]
        public string phoneNumber { get; set; }

        [DataMember]
        public string homeAddress { get; set; }
    }

    [DataContract]
    public class ProfileResponse
    {
        [DataMember]
        public ResponseCode code { get; set; }

        [DataMember]
        public string description { get; set; }
    }
}
