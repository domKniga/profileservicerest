﻿namespace UserProfileService.Model
{
    public static class GenderEnum
    {
        public enum Genders
        {
            Male = 0,
            Female = 1
        }

        public static string stringValue(this Genders gender)
        {
            string value = string.Empty;
            switch (gender)
            {
                case Genders.Male:
                    value = "Мъж";
                    break;
                case Genders.Female:
                    value = "Жена";
                    break;
                default:
                    break;
            }

            return value;
        }
    }
}