﻿using System;
using System.Net;
using System.Text;
using System.Linq;
using System.Windows;
using System.Reflection;
using System.Windows.Input;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using UserProfileService;
using UserProfileService.Model;
using static UserProfileService.Model.GenderEnum;

namespace DemoUserProfileService.ViewModel
{
    public class AddOrUpdateProfileViewModel : INotifyPropertyChanged
    {
        #region PROPERTIES
        private Profile _selectedProfile = new Profile();
        public Profile SelectedProfile
        {
            get { return _selectedProfile; }
            set { _selectedProfile = value; NotifyPropertyChanged(); }
        }

        private ObservableCollection<string> _genders = new ObservableCollection<string>();
        public ObservableCollection<string> Genders
        {
            get { return _genders; }
            set { _genders = value; }
        }

        private bool _isUpdateRequest;
        public bool IsUpdateRequest
        {
            get { return _isUpdateRequest; }
            set { _isUpdateRequest = value; NotifyPropertyChanged(); }
        }

        private string _editModeWindowTitle = string.Empty;
        public string EditModeWindowTitle
        {
            get { return _editModeWindowTitle; }
            set { _editModeWindowTitle = value; }
        }

        private string _editModeButtonTitle = string.Empty;
        public string EditModeButtonTitle
        {
            get { return _editModeButtonTitle; }
            set { _editModeButtonTitle = value; }
        }
        #endregion

        public AddOrUpdateProfileViewModel(Profile selectedProfile)
        {
            LoadGenders();

            IsUpdateRequest = (selectedProfile != null);

            EditModeWindowTitle = IsUpdateRequest ? "Редактиране" : "Добавяне";
            EditModeButtonTitle = IsUpdateRequest ? "Редактирай" : "Добави";
            SelectedProfile = IsUpdateRequest ? selectedProfile : new Profile();

            CommitCommand = new TemplateCommand(Commit, HasMinimumRequiredFields);
            ClearProfileCommand = new TemplateCommand(ClearProfile, param => true);
        }

        #region METHODS
        private void LoadGenders()
        {
            Genders[] genders = (Genders[])Enum.GetValues(typeof(Genders));

            Genders.Add(string.Empty);
            genders.ToList().ForEach(gender => Genders.Add(gender.stringValue()));
        }

        private void Commit(object commandParameter)
        {
            ProfileResponse response = new ProfileResponse();

            string methodName = (IsUpdateRequest ? "UpdateProfile" : "AddProfile");
            string serviceRequestUrl = ProfileService.SERVICE_BASE_URL + methodName;

            using (var webClient = new WebClient() { Encoding = Encoding.UTF8 })
            {
                //Neccessary for POST + JSON(if both request and response format is JSON)
                webClient.Headers[HttpRequestHeader.ContentType] = "application/json";

                string jsonRequest = JsonConvert.SerializeObject(SelectedProfile);

                string jsonResponse = webClient.UploadString(serviceRequestUrl, jsonRequest);

                response = JsonConvert.DeserializeObject<ProfileResponse>(jsonResponse);
            }

            if (response.code != ResponseCode.Ok)
            {
                MessageBox.Show(response.description, "ГРЕШКА", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            //Clear fields in case of addition, keep them otherwise(update case)
            if (string.Equals(methodName, "AddProfile"))
            {
                ClearProfile(null);
            }

            MessageBox.Show(response.description, "ИНФО", MessageBoxButton.OK, MessageBoxImage.Information);

            MessageBoxResult choice = MessageBox.Show("Излизане от прозореца?", "ПОТВЪРЖДЕНИЕ", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (choice == MessageBoxResult.Yes)
            {
                // Close window
                App.Current.Windows.OfType<View.AddOrUpdateProfileWindow>().FirstOrDefault().Close();
            }
        }

        private void ClearProfile(object commandParameter)
        {
            SelectedProfile = new Profile();
        }
        #endregion

        #region COMMAND PROPERTIES
        private ICommand _commitCommand;
        public ICommand CommitCommand
        {
            get { return _commitCommand; }
            set { _commitCommand = value; }
        }

        private ICommand _clearProfileCommand;
        public ICommand ClearProfileCommand
        {
            get { return _clearProfileCommand; }
            set { _clearProfileCommand = value; }
        }
        #endregion

        #region HANDLERS
        private bool HasMinimumRequiredFields(object commandParameter)
        {
            bool requiredPresent = RequiredPresent(SelectedProfile);
            bool minimumPresent = !string.IsNullOrEmpty(SelectedProfile.personalNumber);

            return (IsUpdateRequest && minimumPresent) || requiredPresent;
        }

        private bool RequiredPresent(object myObject)
        {
            foreach (PropertyInfo pi in myObject.GetType().GetProperties())
            {
                if (pi.PropertyType == typeof(string) && !string.Equals(pi.Name, "phoneNumber") && !string.Equals(pi.Name, "homeAddress"))
                {
                    string value = (string)pi.GetValue(myObject);
                    if (string.IsNullOrEmpty(value))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
        #endregion
    }
}
