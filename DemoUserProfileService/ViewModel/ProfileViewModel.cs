﻿using System;
using System.IO;
using System.Net;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using UserProfileService;
using UserProfileService.Model;
using DemoUserProfileService.View;
using DemoUserProfileService.Filters;
using static UserProfileService.Model.GenderEnum;

namespace DemoUserProfileService.ViewModel
{
    public class ProfileViewModel : INotifyPropertyChanged
    {
        #region CONSTANTS
        protected static readonly SolidColorBrush FILTER_VALUE_PRESENT_COLOR = new SolidColorBrush(SystemColors.WindowBrush.Color);
        protected static readonly SolidColorBrush FILTER_VALUE_NOT_PRESENT_COLOR = new SolidColorBrush(Colors.NavajoWhite);
        #endregion

        #region PROPERTIES
        private string _selectedFilter = string.Empty;
        public string SelectedFilter
        {
            get { return _selectedFilter; }
            set { _selectedFilter = value; NotifyPropertyChanged(); }
        }

        private ObservableCollection<string> _filters = new ObservableCollection<string>();
        public ObservableCollection<string> Filters
        {
            get { return _filters; }
            set { _filters = value; }
        }

        private Profile _selectedProfile = null;
        public Profile SelectedProfile
        {
            get { return _selectedProfile; }
            set { _selectedProfile = value; NotifyPropertyChanged(); }
        }

        private ObservableCollection<Profile> _profiles = new ObservableCollection<Profile>();
        public ObservableCollection<Profile> Profiles
        {
            get { return _profiles; }
            set { _profiles = value; }
        }

        private ObservableCollection<string> _enumeratedValues = new ObservableCollection<string>();
        public ObservableCollection<string> EnumeratedValues
        {
            get { return _enumeratedValues; }
            set { _enumeratedValues = value; }
        }

        private string _selectedFilterByValue = string.Empty;
        public string SelectedFilterByValue
        {
            get { return _selectedFilterByValue; }
            set { _selectedFilterByValue = value; NotifyPropertyChanged(); }
        }

        private string _typedFilterByValue = string.Empty;
        public string TypedFilterByValue
        {
            get { return _typedFilterByValue; }
            set { _typedFilterByValue = value; NotifyPropertyChanged(); }
        }

        private Visibility _tbFilterValueVisibility = Visibility.Visible;
        public Visibility TbFilterValueVisibility
        {
            get { return _tbFilterValueVisibility; }
            set { _tbFilterValueVisibility = value; NotifyPropertyChanged(); }
        }

        private Visibility _cbFilterValueVisibility = Visibility.Collapsed;
        public Visibility CbFilterValueVisibility
        {
            get { return _cbFilterValueVisibility; }
            set { _cbFilterValueVisibility = value; NotifyPropertyChanged(); }
        }

        private SolidColorBrush _filterValueElementColor = FILTER_VALUE_NOT_PRESENT_COLOR;
        public SolidColorBrush FilterValueElementColor
        {
            get { return _filterValueElementColor; }
            set { _filterValueElementColor = value; NotifyPropertyChanged(); }
        }

        private bool _filterValueElementEnabled = true;
        public bool FilterValueElementEnabled
        {
            get { return _filterValueElementEnabled; }
            set { _filterValueElementEnabled = value; NotifyPropertyChanged(); }
        }

        private int _profilesTotalCount = 0;
        public int ProfilesTotalCount
        {
            get { return _profilesTotalCount; }
            set { _profilesTotalCount = value; NotifyPropertyChanged(); }
        }
        #endregion

        public ProfileViewModel()
        {
            LoadFilters();
            LoadGenders();
            RefreshTotalCount();

            FilterCommand = new TemplateCommand(Filter, IsFilterSelected);
            ClearCommand = new TemplateCommand(Clear, IsFilterSelected);
            SwitchFilterValueElementCommand = new TemplateCommand(SwitchFilterValueElement, IsFilterSelected);
            ToggleHighlightCommand = new TemplateCommand(ToggleHighlight, parameter => true);

            AddOrUpdateProfileCommand = new TemplateCommand(AddOrUpdateProfile, parameter => true);
            DeleteProfileCommand = new TemplateCommand(DeleteProfile, parameter => (SelectedProfile != null));
        }

        #region METHODS
        private void LoadFilters()
        {
            ProfileFilter.GetNames().ForEach(filterName => Filters.Add(filterName));
        }

        private void LoadGenders()
        {
            Genders[] genders = (Genders[])Enum.GetValues(typeof(Genders));

            EnumeratedValues.Add(string.Empty);
            genders.ToList().ForEach(gender => EnumeratedValues.Add(gender.stringValue()));
        }

        private void RefreshTotalCount()
        {
            ProfilesTotalCount = new NoFilter().ExecuteFilter<Profile>(string.Empty).Count();
        }

        private void Filter(object commandParameter)
        {
            string filterName = SelectedFilter;
            string filterByValue = (TbFilterValueVisibility != Visibility.Collapsed) ? TypedFilterByValue : SelectedFilterByValue;

            if (string.IsNullOrEmpty(filterByValue) && (TbFilterValueVisibility == Visibility.Visible || CbFilterValueVisibility == Visibility.Visible))
            {
                MessageBox.Show("Не е позволено филтриране без въведена стойност!", "ГРЕШКА", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            IFilter activeFilter = ProfileFilter.GetInstance(filterName);

            IEnumerable<Profile> filteredProfiles = activeFilter.ExecuteFilter<Profile>(filterByValue);
            if (filteredProfiles == null)
            {
                string[] errors = File.ReadAllLines(ProfileService.SERVICE_FAILURE_LOG);
                string errorReport = string.Join("\n", errors);

                MessageBox.Show(errorReport, "ГРЕШКА", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            ListProfilesOrderedByLastName(filteredProfiles);
        }

        private void ListProfilesOrderedByLastName(IEnumerable<Profile> filteredProfiles)
        {
            Profiles.Clear();
            filteredProfiles.OrderBy(profile => profile.lastName).ToList().ForEach(profile => Profiles.Add(profile));

            if (filteredProfiles.Count() == 0)
            {
                MessageBox.Show("Hяма профили отговарящи на зададения критериий.", "ИНФО", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void Clear(object commandParameter)
        {
            Profiles.Clear();
            TypedFilterByValue = string.Empty;
            SelectedFilterByValue = string.Empty;
        }

        private void SwitchFilterValueElement(object commandParameter)
        {
            if (string.Equals(new GenderFilter().Name, SelectedFilter))
            {
                ShowComboBoxForFilterValue();
            }
            else if (string.Equals(new NoFilter().Name, SelectedFilter))
            {
                ShowNothingForFilterValue();
            }
            else
            {
                ShowTextBoxForFilterValue();
            }
        }

        private void ShowComboBoxForFilterValue()
        {
            SelectedFilterByValue = string.Empty;
            TbFilterValueVisibility = Visibility.Collapsed;
            CbFilterValueVisibility = Visibility.Visible;
        }

        private void ShowTextBoxForFilterValue()
        {
            TypedFilterByValue = string.Empty;
            CbFilterValueVisibility = Visibility.Collapsed;
            TbFilterValueVisibility = Visibility.Visible;
        }

        private void ShowNothingForFilterValue()
        {
            SelectedFilterByValue = string.Empty;
            TypedFilterByValue = string.Empty;
            CbFilterValueVisibility = CbFilterValueVisibility == Visibility.Visible ? Visibility.Hidden : Visibility.Collapsed;
            TbFilterValueVisibility = TbFilterValueVisibility == Visibility.Visible ? Visibility.Hidden : Visibility.Collapsed;
        }

        private void ToggleHighlight(object commandParameter)
        {
            bool isValuePresent = false;

            if (TbFilterValueVisibility == Visibility.Visible)
            {
                isValuePresent = !string.IsNullOrEmpty(TypedFilterByValue);
            }

            FilterValueElementColor = isValuePresent ? FILTER_VALUE_PRESENT_COLOR : FILTER_VALUE_NOT_PRESENT_COLOR;
        }

        private void AddOrUpdateProfile(object commandParameter)
        {
            AddOrUpdateProfileWindow additionWindow = new AddOrUpdateProfileWindow();
            additionWindow.DataContext = new AddOrUpdateProfileViewModel(SelectedProfile);
            additionWindow.ShowDialog();

            this.Refresh();
        }

        private void DeleteProfile(object commandParameter)
        {
            MessageBoxResult choice = MessageBox.Show("Сигурни ли сте че искате да премахнете профила?", "ПОТВЪРЖДЕНИЕ", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
            if(choice != MessageBoxResult.Yes)
            {
                return;
            }

            StringBuilder serviceRequestBuilder = new StringBuilder();
            serviceRequestBuilder
                .Append(ProfileService.SERVICE_BASE_URL)
                .Append("DeleteProfile")
                .Append("?")
                .Append("personalNumber")
                .Append("=")
                .Append(SelectedProfile.personalNumber);

            ProfileResponse response = new ProfileResponse();

            using (var webClient = new WebClient() { Encoding = Encoding.UTF8 })
            {
                //Neccessary for POST + JSON(if both request and response format is JSON)
                webClient.Headers[HttpRequestHeader.ContentType] = "application/json";

                string jsonRequest = JsonConvert.SerializeObject(SelectedProfile.personalNumber);

                string jsonResponse = webClient.UploadString(serviceRequestBuilder.ToString(), jsonRequest);

                response = JsonConvert.DeserializeObject<ProfileResponse>(jsonResponse);
            }

            if (response.code != ResponseCode.Ok)
            {
                MessageBox.Show(response.description, "ГРЕШКА", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            MessageBox.Show(response.description, "ИНФО", MessageBoxButton.OK, MessageBoxImage.Information);

            this.Refresh();
        }

        private void Refresh()
        {
            SelectedFilter = new NoFilter().Name;
            Filter(null);
            RefreshTotalCount();
        }
        #endregion

        #region COMMAND PROPERTIES
        private ICommand _switchFilterValueElementCommand;
        public ICommand SwitchFilterValueElementCommand
        {
            get { return _switchFilterValueElementCommand; }
            set { _switchFilterValueElementCommand = value; }
        }

        private ICommand _toggleHighlightCommand;
        public ICommand ToggleHighlightCommand
        {
            get { return _toggleHighlightCommand; }
            set { _toggleHighlightCommand = value; }
        }

        private ICommand _filterCommand;
        public ICommand FilterCommand
        {
            get { return _filterCommand; }
            set { _filterCommand = value; }
        }

        private ICommand _clearCommand;
        public ICommand ClearCommand
        {
            get { return _clearCommand; }
            set { _clearCommand = value; }
        }

        private ICommand _addOrUpdateProfileCommand;
        public ICommand AddOrUpdateProfileCommand
        {
            get { return _addOrUpdateProfileCommand; }
            set { _addOrUpdateProfileCommand = value; }
        }

        private ICommand _deleteProfileCommand;
        public ICommand DeleteProfileCommand
        {
            get { return _deleteProfileCommand; }
            set { _deleteProfileCommand = value; }
        }
        #endregion

        #region HANDLERS
        private bool IsFilterSelected(object commandParameter)
        {
            return !string.IsNullOrEmpty(SelectedFilter);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
        #endregion
    }

}
