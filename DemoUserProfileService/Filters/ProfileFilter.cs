﻿using System.Net;
using System.Text;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using UserProfileService;
using UserProfileService.Model;
using Newtonsoft.Json;

namespace DemoUserProfileService.Filters
{
    public abstract class ProfileFilter : IFilter
    {
        #region CONSTANTS
        private static readonly List<IFilter> INSTANCES = new List<IFilter>();
        static ProfileFilter()
        {
            Assembly.GetExecutingAssembly().GetTypes()
                .Where(type => typeof(IFilter).IsAssignableFrom(type) && !type.IsAbstract && !type.IsInterface)
                .ToList()
                .ForEach(type => INSTANCES.Add((IFilter)type.GetConstructors().ElementAt(0).Invoke(null)));
        }
        #endregion

        #region PROPERTIES
        public abstract string Name { get; }
        public abstract string ServiceMethodName { get; }
        public abstract string ServiceMethodParameterName { get; }
        #endregion

        #region METHODS
        public override string ToString()
        {
            return Name;
        }

        public static List<string> GetNames()
        {
            List<string> filterNames = new List<string>();
            filterNames.Add(string.Empty);

            INSTANCES.ForEach(filter => filterNames.Add(filter.ToString()));

            filterNames.Sort();

            return filterNames;
        }

        public static IFilter GetInstance(string filterName)
        {
            return INSTANCES.Where(filter => string.Equals(filter.Name, filterName)).FirstOrDefault();
        }

        public IEnumerable<T> ExecuteFilter<T>(string filterByValue)
        {
            string serviceRequestUrl = BuildServiceRequestUrl(filterByValue);

            IEnumerable<Profile> filteredItems = Enumerable.Empty<Profile>();

            using (var webClient = new WebClient() { Encoding = Encoding.UTF8 })
            {
                var json = webClient.DownloadString(serviceRequestUrl);

                filteredItems = JsonConvert.DeserializeObject<IEnumerable<Profile>>(json);
            }

            return (IEnumerable<T>)filteredItems;
        }

        protected string BuildServiceRequestUrl(string filterByValue)
        {
            StringBuilder serviceRequestBuilder = new StringBuilder();
            serviceRequestBuilder
                .Append(ProfileService.SERVICE_BASE_URL)
                .Append(ServiceMethodName)
                .Append("?")
                .Append(ServiceMethodParameterName)
                .Append("=")
                .Append(filterByValue);

            //Handle all records filter
            string serviceRequestUrl = serviceRequestBuilder.ToString();
            if (string.IsNullOrEmpty(ServiceMethodParameterName))
            {
                serviceRequestUrl = serviceRequestUrl.Substring(0, serviceRequestUrl.IndexOf('?'));
            }

            return serviceRequestBuilder.ToString();
        }
        #endregion
    }
}
