﻿namespace DemoUserProfileService.Filters
{
    public class LastNameFilter : ProfileFilter
    {
        public override string Name
        {
            get
            {
                return "Фамилия";
            }
        }

        public override string ServiceMethodName
        {
            get
            {
                return "GetAllForLastName";
            }
        }

        public override string ServiceMethodParameterName
        {
            get
            {
                return "lastName";
            }
        }
    }
}
