﻿using System.Collections.Generic;

namespace DemoUserProfileService.Filters
{
    public interface IFilter
    {
        string Name { get; }
        string ServiceMethodName { get; }
        string ServiceMethodParameterName { get; }

        IEnumerable<T> ExecuteFilter<T>(string filterByValue);
    }
}
