﻿namespace DemoUserProfileService.Filters
{
    public class NoFilter : ProfileFilter
    {
        public override string Name
        {
            get
            {
                return "Всички";
            }
        }

        public override string ServiceMethodName
        {
            get
            {
                return "GetAll";
            }
        }

        public override string ServiceMethodParameterName
        {
            get
            {
                return string.Empty;
            }
        }
    }
}
