﻿namespace DemoUserProfileService.Filters
{
    public class GenderFilter : ProfileFilter
    {
        public override string Name
        {
            get
            {
                return "Пол";
            }
        }

        public override string ServiceMethodName
        {
            get
            {
                return "GetAllForGender";
            }
        }

        public override string ServiceMethodParameterName
        {
            get
            {
                return "genderName";
            }
        }
    }
}
