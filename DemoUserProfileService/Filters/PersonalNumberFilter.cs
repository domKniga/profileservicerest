﻿namespace DemoUserProfileService.Filters
{
    public class PersonalNumberFilter : ProfileFilter
    {
        public override string Name
        {
            get
            {
                return "ЕГН";
            }
        }

        public override string ServiceMethodName
        {
            get
            {
                return "GetSpecific";
            }
        }

        public override string ServiceMethodParameterName
        {
            get
            {
                return "personalNumber";
            }
        }

    }
}
