﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;

namespace DemoUserProfileService.View
{
    public partial class AddOrUpdateProfileWindow : Window
    {
        private readonly SolidColorBrush FILTER_VALUE_PRESENT_COLOR = new SolidColorBrush(SystemColors.WindowBrush.Color);
        private readonly SolidColorBrush FILTER_VALUE_NOT_PRESENT_COLOR = new SolidColorBrush(Colors.NavajoWhite);

        public AddOrUpdateProfileWindow()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = sender as TextBox;

            textBox.Background = string.IsNullOrEmpty(textBox.Text) ? FILTER_VALUE_NOT_PRESENT_COLOR : FILTER_VALUE_PRESENT_COLOR;
        }
    }
}
